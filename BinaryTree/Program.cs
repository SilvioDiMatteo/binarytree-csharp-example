﻿using System;

namespace BinaryTree
{
    class Program
    {
        static void Main(string[] args)
        {
            BinaryTree binaryTree = new BinaryTree();
            Random random = new Random();

            for (int i = 0; i < 100; i++)
            {
                int randomNumber = random.Next(30);
                if (binaryTree.Insert(new BinaryTreeNode(randomNumber)))
                {
                    Console.WriteLine("Generated number: " + randomNumber);
                }
            }

            int dataToCheck = 15;
            Console.WriteLine("Does the tree contain the value {0}? {1}", dataToCheck, binaryTree.Contains(dataToCheck));
        }
    }    
}
