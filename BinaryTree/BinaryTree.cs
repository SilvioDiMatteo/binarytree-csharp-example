﻿using System;

namespace BinaryTree
{
    /// <summary>
    /// A simple C# Binary Tree
    /// </summary>
    class BinaryTree
    {
        /// <summary>
        /// The tree root
        /// </summary>
        BinaryTreeNode root;

        public BinaryTree()
        {
            root = null;
        }

        public BinaryTree(BinaryTreeNode rootNode)
        {
            root = rootNode;
        }

        /// <summary>
        /// Inserts a new node in the tree
        /// </summary>
        /// <param name="node">The new tree node</param>
        /// <returns>Has been the node inserted in the tree?</returns>
        public bool Insert(BinaryTreeNode node)
        {
            if (node == null) { throw new ArgumentNullException("The TreeNode to insert can't be null"); }
            if (root == null)
            {
                root = node;
                return true;
            }

            bool inserted = false;
            BinaryTreeNode target = root;
            while (!inserted)
            {
                if (node.data > target.data)
                {
                    if (target.right != null)
                    {
                        target = target.right;
                        continue;
                    }
                    else
                    {
                        target.right = node;
                        target.right.SetParent(target);
                        inserted = true;
                    }
                }
                else if (node.data < target.data)
                {
                    if (target.left != null)
                    {
                        target = target.left;
                        continue;
                    }
                    else
                    {
                        target.left = node;
                        target.left.SetParent(target);
                        inserted = true;
                    }
                }
                else
                {
                    break;
                }
            }
            return inserted;
        }

        /// <summary>
        /// Searchs for a tree node
        /// </summary>
        /// <param name="data">The data of the searched node</param>
        /// <returns>The occurrency of the found node</returns>
        public BinaryTreeNode Search(int data)
        {
            if (root == null)
            {
                return null;
            }
            BinaryTreeNode target = root;

            while (target != null)
            {
                if (data > target.data)
                {
                    target = target.right;
                }
                else if (data < target.data)
                {
                    target = target.left;
                }
                else
                {
                    return target;
                }
            }
            return null;
        }

        /// <summary>
        /// Removes a node from the tree
        /// </summary>
        /// <param name="data">The data of the node to remove</param>
        public void Remove(int data)
        {
            if (root == null)
            {
                return;
            }
            BinaryTreeNode target = Search(data);
            if (target == null) { throw new Exception("There isn't a node to remove, with the data " + data); }

            target.Remove();
            if (target.left != null)
            {
                Insert(target.left);
            }
            if (target.right != null)
            {
                Insert(target.right);
            }
        }

        /// <summary>
        /// Checks if the tree contains a node with a certain data in it
        /// </summary>
        /// <param name="data">The data of the searched node</param>
        /// <returns></returns>
        public bool Contains(int data)
        {
            return Search(data) != null;
        }
    }

    /// <summary>
    /// Represents a Node of the Binary Tree
    /// </summary>
    class BinaryTreeNode
    {
        /// <summary>
        /// The direct parent of this node (Is null if this node is the root of the tree)
        /// </summary>
        BinaryTreeNode parent;
        /// <summary>
        /// The left child of the node
        /// </summary>
        public BinaryTreeNode left;
        /// <summary>
        /// The right child of the node
        /// </summary>
        public BinaryTreeNode right;
        /// <summary>
        /// The data contained by this node
        /// </summary>
        public int data;

        public BinaryTreeNode(int newData)
        {
            data = newData;
        }
        
        /// <summary>
        /// Sets the node parent
        /// </summary>
        /// <param name="newParent">The new node parent</param>
        public void SetParent(BinaryTreeNode newParent)
        {
            parent = newParent;
        }

        /// <summary>
        /// Removes this node from the tree
        /// </summary>
        public void Remove()
        {
            if (parent == null) { return; }
            if (parent.right != null && parent.right == this)
            {
                parent.right = null;
                return;
            }
            if (parent.left == null) { return; }
            parent.left = null;
        }
    }
}
